/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Anna2
 */
public class KeyElementModel extends DefaultTableModel {
    private final ArrayList<KeyData> data = new ArrayList<>();
    private final String[] colNames = {"Название","Выбрать","Ввод значений"};
    private final Class[] colClasses = {String.class,Boolean.class,String.class,};

    public KeyElementModel() {
    }
    
    public void addCheck(KeyData chd){
        data.add(chd);
        fireTableDataChanged();
    }

    public ArrayList<KeyData> getData() {
        return data;
    }

    @Override
    public void setValueAt(Object aValue, int row, int column) {
        if(data ==null || data.isEmpty())return ;
        KeyData ch = data.get(row);
        switch(column){
            case 2: ch.setValue((String)aValue);
            break;
            /*case 3: ch.setUpper((String)aValue);
            break;*/
            case 1: ch.setChecked((Boolean)aValue);
            break;
            default: 
        }
    }

    @Override
    public Object getValueAt(int row, int column) {
        if(data == null || data.isEmpty())return null;
        KeyData ch = data.get(row);
        switch(column){
            case 1:return ch.isChecked();
            case 2: return ch.getValue();
            //case 3: return ch.getUpper();
            default: return ch.getName();
        }
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return column != 0;
    }

    @Override
    public String getColumnName(int column) {
        return colNames[column];
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public int getRowCount() {
        if(data == null || data.isEmpty())return 0;
        return data.size();
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return colClasses[columnIndex];
    }
}
