/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.apache.commons.lang.StringUtils;
import org.jfree.chart.title.TextTitle;
import structuring.FileContent;
import yaodinterface.ParserFrame;
import yaodprocessing.YaodProcessor;

/**
 * класс содержит вспомогательные методы для кнопок фремов
 *
 * @author Anna2
 */
public class UtilsManager {

    public List<FileContent> listFileContent;

    public UtilsManager() {
    }

    /**
     * Попытка отфильтровать массив объектов FileContent для того, чтобы
     * получить объекты, связанные с теми номерами строк, которые содержат
     * введенные пользователем через таблицу в ChartFrame параметры.
     *
     * @param listFileContent список объектов FileCONTENT? которые надо
     * отфильтровать
     * @param keymod модель данных таблицы, в которую вносятся значения
     * @return отфильтрованный listFileContent
     */
    public List<FileContent> getFilteredFileContent(List<FileContent> listFileContent, KeyElementModel keymod) {
        if (listFileContent == null) {
            return null;
        }
        List<Integer> restriction = new ArrayList<>();
        ArrayList<KeyData> data = keymod.getData();

        if ((data != null) && (data.size() > 0)) {
            for (KeyData d : data) {
                if (d.isChecked() == false) {
                    try {
                        if (d.getValue().trim().isEmpty() == false) {
                            int lower = 0;
                            int upper = 0;
                            boolean containsNumberLow = d.getValue().matches("\\d+");
                            System.out.println("----------------------------------");
                            System.out.println("обработка::::::: name=" + d.getName() + ", getValue=" + d.getValue());
                            if (d.getValue().trim().isEmpty() == false && containsNumberLow) {
                                lower = Integer.valueOf(d.getValue());
                                upper = Integer.valueOf(d.getValue());
                            } else {
                                JOptionPane.showMessageDialog(null, "Заполните все поля, кроме выбранного!",
                                        "Оповещение", 0);
                            }
                            for (FileContent fc : listFileContent) {
                                boolean flag = false;
                                if (fc.getNameField().contains(d.getName())) {
                                    int value = Integer.valueOf(fc.getValueField());
                                    if ((value == lower)) {
                                        flag = true;
                                        if (restriction.contains(fc.getStrNum()) == false) {
                                            restriction.add(fc.getStrNum());
                                        }

                                    } else {
                                        flag = false;
                                    }
                                    /*System.out.println("-> элемент = " + fc.getNameField() + ", значение = " + fc.getValueField()
                                            + ", lower=" + lower + ", upper=" + upper + ", flag = " + flag + ", int value = " + value);*/
                                }
                            }

                            List<FileContent> content = getNewContentTable(listFileContent, restriction);
                            listFileContent = new ArrayList<>(content);
                            restriction.clear();

                        }
                    } catch (Exception e) {
                        java.util.logging.Logger.getLogger(ParserFrame.class.getName()).log(Level.SEVERE, null, e);
                        e.printStackTrace();
                    }
                } else {
                    continue;
                }
                //  }

                System.out.println("-----------------------------------------");
            }

        } else {
            java.util.logging.Logger.getLogger(ParserFrame.class.getName())
                    .log(Level.INFO, null, "sortdata is null or empty");
            System.out.println("data =" + data);
        }
        return listFileContent;
    }

    public List<FileContent> getNewContentTable(List<FileContent> listFileContent, List<Integer> restriction) {
        List<FileContent> result = new ArrayList<>();
        try {
            System.out.println("[getNewContentTable]before removing from listFileContent, size = "
                    + listFileContent.size());
            if (listFileContent != null) {
                for (FileContent fc : listFileContent) {
                    if (restriction.contains(fc.getStrNum()) == true) {
                        //удалить все объекты с  номером строки, которого нет в restriction
                        result.add(fc);
                    }
                }
            } else {
                java.util.logging.Logger.getLogger(ParserFrame.class.getName()).log(Level.INFO,
                        null, "listFileContent is null");
            }
            System.out.println("[getNewContentTable]after removing from listFileContent, listFileContent = " + listFileContent.size());
        } catch (Exception e) {
            java.util.logging.Logger.getLogger(ParserFrame.class.getName()).log(Level.SEVERE,
                    null, e);
        }
        return result;
    }

    public List<FileContent> getFilteredFileContent(List<FileContent> listFileContent, SortModel model) {
        if (listFileContent == null) {
            return null;
        }
        List<Integer> restriction = new ArrayList<>();
        ArrayList<SortData> data = model.getData();
        if ((data != null) && (data.size() > 0)) {
            for (SortData d : data) {
                if (d.isChecked() == true) {
                    try {
                        if ((d.getLower().trim().isEmpty() == false) && (d.getUpper().trim().isEmpty() == false)) {
                            int lower = 0;
                            int upper = 0;
                            boolean containsNumberLow = d.getLower().matches("\\d+");
                            boolean containsNumberUp = d.getUpper().matches("\\d+");
                            if (!d.getLower().trim().isEmpty() && !d.getUpper().trim().isEmpty()
                                    && containsNumberLow && containsNumberUp) {
                                lower = Integer.valueOf(d.getLower());
                                upper = Integer.valueOf(d.getUpper());
                                if (lower > upper) {
                                    JOptionPane.showMessageDialog(null, d.getName() + " ДО больше, чем " + d.getName() + " ПОСЛЕ", "Сообщение", 0);
                                }
                            } else {
                                int low = 0, top = 0;
                                for (int i = 0; i < listFileContent.size(); i++) {
                                    if (StringUtils.containsIgnoreCase(listFileContent.get(i).getNameField(), d.getName())) {
                                        int newlow = Integer.parseInt(listFileContent.get(i).getValueField());
                                        if (newlow < low) {
                                            low = newlow;
                                        }
                                    }
                                    break;
                                }
                                for (int i = 0; i < listFileContent.size(); i++) {
                                    if (StringUtils.containsIgnoreCase(listFileContent.get(i).getNameField(), d.getName())) {
                                        int newtop = Integer.parseInt(listFileContent.get(i).getValueField());
                                        if (newtop > top) {
                                            top = newtop;
                                        }
                                    }
                                }
                                if (d.getLower() == "") {
                                    lower = low;
                                    upper = Integer.parseInt(d.getUpper());
                                } else if (d.getUpper() == "") {
                                    upper = top;
                                    lower = Integer.parseInt(d.getLower());
                                }
                            }
                            for (FileContent fc : listFileContent) {
                                boolean flag = false;
                                if (fc.getNameField().contains(d.getName())) {
                                    int value = Integer.valueOf(fc.getValueField());
                                    if ((value >= lower) && (value <= upper)) {
                                        flag = true;
                                        if (restriction.contains(fc.getStrNum()) == false) {
                                            restriction.add(fc.getStrNum());
                                        }

                                    } else {
                                        flag = false;
                                    }

                                }
                            }

                            List<FileContent> content = getNewContentTable(listFileContent, restriction);
                            listFileContent = new ArrayList<>(content);
                            restriction.clear();

                        }
                    } catch (Exception e) {
                        java.util.logging.Logger.getLogger(ParserFrame.class.getName()).log(Level.SEVERE, null, e);
                        e.printStackTrace();
                    }
                } else {
                    continue;
                }

                System.out.println("-----------------------------------------");
            }

        } else {
            java.util.logging.Logger.getLogger(ParserFrame.class.getName())
                    .log(Level.INFO, null, "sortdata is null or empty");
            System.out.println("data =" + data);
        }
        return listFileContent;
    }

    public String getElementMeaning(String element, String yaodFilePath) {
        String meaning = "";
        if ((element == null) || (element.isEmpty())) {
            return "";
        } else {
            try {
                YaodProcessor yp = new YaodProcessor(yaodFilePath);
                meaning = yp.showElementMeaning(element);
                return meaning;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return meaning;
    }

    public int addSpinnerDefaultValue(int size) {
        int result = 0;
        if (size == 0) {
            System.out.println("[addSpinnerDefaultValue]size=" + size);
            return 0;
        }
        try {
            result = (int) (Math.log(size) / Math.log(2.0));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public List<TextTitle> getGraphicParams(ArrayList<SortData> data) {
        // flag просто для того, чтобы отличить 2 метода с одним названием. ТО что AarrayList разных типов компилятору 
        // все равно
        List<TextTitle> graphicParamsList = new ArrayList<>();
        if (data == null) {
            return graphicParamsList;
        }
        try {
            if (data.isEmpty() == false) {
                for (SortData d : data) {
                    if (d.isChecked()) {
                        StringBuffer sb = new StringBuffer(d.getName());
                        sb.append(" ");
                        if (d.getLower().length() > 0) {
                            sb.append("C ").append(d.getLower()).append(" ");
                            if (d.getUpper().length() > 0) {
                                sb.append("по ").append(d.getUpper()).append("; ").append("\n");
                            }
                        } else {
                            if (d.getUpper().length() > 0) {
                                sb.append("До ").append(d.getUpper()).append("; \n");
                            }
                        }
                        System.out.println("sb = " + sb.toString());
                        TextTitle tt = new TextTitle(sb.toString());
                        System.out.println("Text Title = " + tt.getText());
                        graphicParamsList.add(tt);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return graphicParamsList;
    }

    public List<TextTitle> getGraphicParams(ArrayList<KeyData> data, boolean flag) {
        // flag просто для того, чтобы отличить 2 метода с одним названием. ТО что AarrayList разных типов компилятору 
        // все равно
        List<TextTitle> graphicParamsList = new ArrayList<>();
        if (data == null) {
            return graphicParamsList;
        }
        try {
            if (data.isEmpty() == false) {
                for (KeyData d : data) {
                    if (d.isChecked()==false) {
                        StringBuffer sb = new StringBuffer(d.getName());
                        sb.append(" ");
                        if (d.getValue().length() > 0) {
                            sb.append(" = ").append(d.getValue()).append(" ");
                        }
                        TextTitle tt = new TextTitle(sb.toString());
                        graphicParamsList.add(tt);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return graphicParamsList;
    }
    
    public boolean containsEmptyParams(ArrayList<KeyData> data){
        try {
            if(data == null){
                return true;
            }
            if(data.size()>0){
                for(KeyData d: data){
                    if(d.isChecked()==false){
                        if(d.getValue().length()==0){
                            return true;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}
