/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 *
 * @author Anna2
 */
public class KeyData {
     private boolean checked;
    private String name;
    private String value;

    public KeyData(boolean checked, String name, String value) {
        this.checked = checked;
        this.name = name;
        this.value = value;
    }

    public KeyData() {
    }
    
    @Override
    public String toString() {
        return name+" "+checked+" value:"+value;
    }
    
    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
    
}
