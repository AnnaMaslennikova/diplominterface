/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.util.List;

/**
 *
 * @author oam
 */
public class SortData {
    private boolean checked;
    private String name;
    private String lower;
    private String upper;

    public SortData() {
    }

    public SortData(boolean checked, String name, String lower, String upper) {
        this.checked = checked;
        this.lower = lower;
        this.upper = upper;
        this.name = name;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getLower() {
        return lower;
    }

    public void setLower(String lower) {
        this.lower = lower;
    }

    public String getUpper() {
        return upper;
    }

    public void setUpper(String upper) {
        this.upper = upper;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name+" "+checked+" from:"+lower+" to:"+upper;
    }
    
}
