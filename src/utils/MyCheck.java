/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author oam
 */
public class MyCheck extends DefaultTableModel  {
    
    private ArrayList<VisibleDataColumns> data = new ArrayList<>();
    private final String[] colNames = {"Выбор","Колонка"};
    private final Class[] colClasses = {Boolean.class, String.class};
    Vector<String> columnsValues = null;

  /*  public MyCheck(Object[][] data, Object[] columnNames) {
        super(data, columnNames);
    }*/

    public MyCheck() {
    }
        
    public void addData(VisibleDataColumns ch){
        data.add(ch);
        fireTableDataChanged();
    }
    
    public void dumpData(){
        columnsValues = new Vector<>();
        for(VisibleDataColumns ch:data){
            System.out.println(""+ch.isFlag()+" "+ch.getText());
            if (ch.isFlag()==true){
                columnsValues.add(ch.getText());
            }
        }
    }
    
    public List<Integer> receiveRowsToShow(){
        List<Integer> rows = new ArrayList<>();
        for(int i=0; i<data.size(); i++/*VisibleDataColumns ch:data*/){
            if(data.get(i).isFlag()==false){
                rows.add(i);
            }
        }
        return rows;
    }
    
    @Override
    public void setValueAt(Object aValue, int row, int column) {
        if(column != 0)return ;
        VisibleDataColumns ch = data.get(row);
        ch.setFlag((Boolean)aValue);
    }

    @Override
    public Object getValueAt(int row, int column) {
        if(data==null || data.isEmpty())return null;
        VisibleDataColumns ch = data.get(row);
        if(column == 0)return ch.isFlag();
        return ch.getText();
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        if(column == 0)return true;
        return false;
    }

    @Override
    public String getColumnName(int column) {
        return colNames[column];
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public int getRowCount() {
        if(data==null || data.isEmpty())return 0;
        return data.size();
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return colClasses[columnIndex];
    }
    
    
}
