/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author oam
 */
public class SortModel extends DefaultTableModel {
    
    private final ArrayList<SortData> data = new ArrayList<>();
    private final String[] colNames = {"Название","Выбрать","От","До"};
    private final Class[] colClasses = {String.class,Boolean.class,String.class,String.class};
    
    public SortModel() {
    }
    
    public void addCheck(SortData chd){
        data.add(chd);
        fireTableDataChanged();
    }

    public ArrayList<SortData> getData() {
        return data;
    }

    @Override
    public void setValueAt(Object aValue, int row, int column) {
        if(data ==null || data.isEmpty())return ;
        SortData ch = data.get(row);
        switch(column){
            case 2: ch.setLower((String)aValue);
            break;
            case 3: ch.setUpper((String)aValue);
            break;
            case 1: ch.setChecked((Boolean)aValue);
            break;
            default: 
        }
    }

    @Override
    public Object getValueAt(int row, int column) {
        if(data == null || data.isEmpty())return null;
        SortData ch = data.get(row);
        switch(column){
            case 1:return ch.isChecked();
            case 2: return ch.getLower();
            case 3: return ch.getUpper();
            default: return ch.getName();
        }
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return column != 0;
    }

    @Override
    public String getColumnName(int column) {
        return colNames[column];
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public int getRowCount() {
        if(data == null || data.isEmpty())return 0;
        return data.size();
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return colClasses[columnIndex];
    }
    
}
